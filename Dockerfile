# Version 0.0.3
FROM 43edf5af8576
MAINTAINER Nigel Gibbs "nigel@gibbsoft.com"
RUN mkdir -p /opt/sample_app
ADD web_server.rb /opt/sample_app
ADD Gemfile /opt/sample_app/
RUN gem install bundler
RUN cd /opt/sample_app && bundle
CMD ruby /opt/sample_app/web_server.rb
EXPOSE 80
