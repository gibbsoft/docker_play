require 'sinatra'

set :port, 80
set :bind, '0.0.0.0'

get '/' do
  content_type 'text/html'
  '<!DOCTYPE html><html><head></head><body><h1>Hello World!</h1></body></html>'
end
